# Instructions for getting started on this challenge

Welcome to the Filevine Challenge.   

The goal is to evaluate your skills in working with infrastructure, code & pipelines in a development workflow.  


This challenge requires you to fork this project and setup an AWS/Google/Azure site to host your code at a bare minimum.  

## Setup a Cloud Account 

Setup a cloud account if you don't already have one and the expectation is to use the Free Tier resources only
* AWS  - https://aws.amazon.com/free
* Azure - https://azure.microsoft.com/en-us/free/
* Google Cloud - https://console.cloud.google.com/freetrial

## Get a gitlab.com account

Signup for an account on Gitlab.com if you don't already have one to fork the project

https://gitlab.com/users/sign_up

## Fork this project and start working

Then fork this project to your personal Gitlab Account.

1. Go to this link:  https://gitlab.com/fv-interview-v2/challenge-v4
1. In the upper right corner, click "Fork" 
1. Name it what you want and put into your Gitlab Group.  
1. Suggested to set Private.   
1. Work on the challenges. 
1. Let us know if you have any questions, issues or are done with the challenge.   
1. Send us the link to the forked project and we will schedule the next interview.  
