function score(fiveDiceRoll, scoringRule) {
  switch(scoringRule)
  {
    case "chance":
      return scoreChance(fiveDiceRoll);
    case "three-of-a-kind":
      return threeOfAKind(fiveDiceRoll);
    default:
      return -1;
  }
}

// sum of all the items in the array [1,2,3,4,5] === 15
function scoreChance(fiveDiceRoll) {

}

// if 3 or more items in the array  are the same value then sum up all the values in the array [4,4,4,1,1] returns 14
//else return 0 [1,2,2,3,1] returns 0
function threeOfAKind(fiveDiceRoll) {

}
module.exports = score;
