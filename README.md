# Reliability Interviews
## Why are We Hiring and Why This Process?
Being a professional is hard. Not only do you need to be an expert in your field but you then need to worry about how you communicate, manage your time, store and share information, keep your data secure, make your tooling reliable, and a million other details. Filevine dreams of a day when all of these details are taken care of so that professionals can get back to focusing on what they love. 

We are building our team because we recognize that to make good on that dream we need the help of amazingly talented people like you. 

The intent of this interview process is to help both you and Filevine gain confidence that we are a good fit for each other and that establishing a working relationship will be of lasting benefit to everyone. 

## The Process 
The overall process we follow is detailed below. We are trying to optimize for quick decision making so that no one is waiting too long before they know the decision. Either side can end the process at any phase but we aim to complete the whole process in less than 2 weeks. 

**Initial Screening** - At this point one of our recruiters has called you and decided you might be a good fit for one of our open position. 

**SRE Challenge** 

The SRE Challenge is designed to help evaluate multiple things including:
- Your general abilities as a software engineer
- Your ability to follow a good development flow
- Your ability to work in a group and on your own
- Your temperament during a code review.

## The Process 
After your inital screening you will be sent an invite to the SRE Challenge Gitlab repository (if you are reading this you already have access =) ). Inside this Gitlab repository there are a number of issues. You will be given 5 business days to complete the challenge. Your goal is to complete as many of the **gitlab issues** as you can before the deadline. You will likely need to spend an hour and a half to earn a reasonable  score. **NOTE: There are more issues than a good engineer could complete in the timeframe allotted. Do as much as you reasonably can and don't worry about the rest. Also please only turn in what you have working. You will lose points for items in your MR that do not function.** Once the deadline is reached an SRE in the Reliability department will review your submission, including asking questions about the code. When they have completed the review they will score your assignment.

## Scoring 
You will recive points as follows.
* Complete as many issues as you can do in the 5 business days after reciving the challenge - points depend on the tasks completed
  - Created and Worked Against a Branch - 1 point
  - Committed to your branch after each working increment is complete  - 1 point
  - Submitting a merge request when you have completed an issue - 1 point
* Bonus points can be earned for clever solutions, following best practices, and impressive changes made above and beyond the basic task requirments

**First Interview** - This will be with the hiring managers and director.  They will discuss what the position is and what you are looking for.  This will be a 60 min interview.

**Second Interview** - This interview will be with the hiring manager and occasionally other team members they choose to invite. These last a total of 60 minutes. 
- Introductions to each other and to the role - 10 minutes
- General questions about yourself and your experiance - 10 minutes
- Questions about your SRE Challenge submission - Details below - 15 minutes
- Our DevOps & Reliability philosophy - 10 minutes
- An opportunity for you to ask any additional questions you may have - 15 minutes 

**Potential Wider Team Interview** - This interview will be with your hiring manager, the team lead of your perspective team, an SRE, and occasionally individuals from other parts of Filevine - This interview is an opportunity for the wider team to ask ad hoc questions to evaluate if they think you will be a good addition to the team, and is generally far more stressful than the first - 60 minutes 

**The Final Decision** - We try to make deliberations as short as possible and communicate to you in a timely manner if we will make an offer for you to join us or not
